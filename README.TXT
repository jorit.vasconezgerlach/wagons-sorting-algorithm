------------------------------------------------------------------------
READ ME
------------------------------------------------------------------------

PROJECT PURPOSE: Whith this algorithm the wagons with different numbers
should be moved into the right order. There three tracks (as Stacks) to
achive this.

VERSION: public version 001

HOW TO START THIS PROJECT: nothing here yet.

AUTHOR: Jorit Vásconez Gerlach
GitLab: @jorit.vasconezgerlach (https://gitlab.com/jorit.vasconezgerlach)
Instagram: @jorit.520 (https://www.instagram.com/jorit.520/)