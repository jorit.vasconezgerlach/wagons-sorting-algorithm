/*
 * AUTHOR: Jorit Vásconez Gerlach
 * ╚ GitLab: @jorit.vasconezgerlach (https://gitlab.com/jorit.vasconezgerlach)
 * ╚ Instagram: @jorit.520 (https://www.instagram.com/jorit.520/)
 */

public class Railroad_Tracks
{
    // instantiate the three Stacks (the tracks)
    Stack<Wagon> trackA;
    Stack<Wagon> trackB;
    Stack<Wagon> trackC;
    
    /*------- Constructor -------*/
    public Railroad_Tracks() {
        // initialize the thee Stacks (the tracks)
        trackA = new Stack<Wagon>();
        trackB = new Stack<Wagon>();
        trackC = new Stack<Wagon>();
        
        // out print
        System.out.println("╋╋╋╋╋╋╋╋╋╋╋┏┓╋╋╋╋╋┏┓");
        System.out.println("╋╋╋╋╋╋╋╋╋╋┏┛┗┓╋╋╋╋┃┃");
        System.out.println("┏━━┳━┳━━┳━┻┓┏╋━━┳━┛┃");
        System.out.println("┃┏━┫┏┫┃━┫┏┓┃┃┃┃━┫┏┓┃");
        System.out.println("┃┗━┫┃┃┃━┫┏┓┃┗┫┃━┫┗┛┃");
        System.out.println("┗━━┻┛┗━━┻┛┗┻━┻━━┻━━┛");
        System.out.println("•Successfully created this Railroad with 3 tracks.");
    }
    
    /*------- Methods -------*/
    // creates a new wagon and adds it to track a
    public void createWagonOnTrackA(int pWagonNumber)
    {
        // adds the made wagon to the track 'A'
        trackA.push(new Wagon(pWagonNumber));
        
        // clear out print from the class Wagon
        System.out.print('\u000C');
        
        // out print
        System.out.println("╋╋╋╋╋╋╋╋╋╋╋┏┓╋╋╋╋╋┏┓");
        System.out.println("╋╋╋╋╋╋╋╋╋╋┏┛┗┓╋╋╋╋┃┃");
        System.out.println("┏━━┳━┳━━┳━┻┓┏╋━━┳━┛┃");
        System.out.println("┃┏━┫┏┫┃━┫┏┓┃┃┃┃━┫┏┓┃");
        System.out.println("┃┗━┫┃┃┃━┫┏┓┃┗┫┃━┫┗┛┃");
        System.out.println("┗━━┻┛┗━━┻┛┗┻━┻━━┻━━┛");
        System.out.println("•Successfully created wagon " + pWagonNumber + " on the track A.");
        
        // print current info
        System.out.println("---------------------------------------------");
        printCurrentInfo();
    }
    
    // choose a existing wagon and adds it to track a
    public void addWagonOnTrackA(Wagon pWagon)
    {
        // verification, that the wagon has is not null
        if(pWagon != null) {
            // adds the existing wagons to the track 'A'
            trackA.push(pWagon);
            
            // out print 'added'
            System.out.println("╋╋╋╋╋┏┓╋┏┓╋╋╋╋┏┓");
            System.out.println("╋╋╋╋╋┃┃╋┃┃╋╋╋╋┃┃");
            System.out.println("┏━━┳━┛┣━┛┣━━┳━┛┃");
            System.out.println("┃┏┓┃┏┓┃┏┓┃┃━┫┏┓┃");
            System.out.println("┃┏┓┃┗┛┃┗┛┃┃━┫┗┛┃");
            System.out.println("┗┛┗┻━━┻━━┻━━┻━━┛");
            System.out.println("•Successfully added wagon " + pWagon.getWagonNummer() + " to the track A.");
        } else {
            // out print 'error'
            System.out.println("██████████████████████████████");
            System.out.println("█▄─▄▄─█▄─▄▄▀█▄─▄▄▀█─▄▄─█▄─▄▄▀█");
            System.out.println("██─▄█▀██─▄─▄██─▄─▄█─██─██─▄─▄█");
            System.out.println("▀▄▄▄▄▄▀▄▄▀▄▄▀▄▄▀▄▄▀▄▄▄▄▀▄▄▀▄▄▀");
            System.out.println("•What happed?");
            System.out.println("The added wagon has no number.");
            System.out.println();
            System.out.println("•What can I do?");
            System.out.println("Try removing the object and create a new one.");
        }

        // print current info
        System.out.println("---------------------------------------------");
        printCurrentInfo();
    }

    // move a wagon from one to an other track
    private void changeToTrack(Stack<Wagon> pTrack1, Stack<Wagon> pTrack2) {
        // take the highest wagon from 'Track1'
        Wagon highestWagonTrack1 = pTrack1.top();

        // adds the highest wagon from 'Track1' on the top of 'Track2'
        pTrack2.push(highestWagonTrack1);

        // removes the highest wagon from 'Track1'
        pTrack1.pop();
    }
    
    // compare highest wagons from two tracks
    private boolean wagonIsSmallerAs(Stack<Wagon> pTrack1, Stack<Wagon> pTrack2) {
        // instantiate the boolean track1IsSmallerTrack2
        boolean track1IsSmallerTrack2 = false;

        // take the highes wagon from 'Track1' and 'Track2'
        Wagon highestWagonTrack1 = (Wagon) pTrack1.top();
        Wagon highestWagonTrack2 = (Wagon) pTrack2.top();
        
        // tests, if one of the Tracks is empty
        if(pTrack1.isEmpty() || pTrack2.isEmpty()) {
            // tests, if both are empty
            if(pTrack1.isEmpty() && !pTrack2.isEmpty()) {
                track1IsSmallerTrack2 = true;
            }
        }

        // test, if 'Track2' has the higher number
        else if(highestWagonTrack1.getWagonNummer() < highestWagonTrack2.getWagonNummer()) {
            track1IsSmallerTrack2 = true;
        }

        // if 'Track1' has the higher number it's already on false
        
        // returns the result of the comparation
        return track1IsSmallerTrack2;
    }
    
    // algorithm that sorts the wagons to track 'C'
    public void sortWagons() {
        while(!trackA.isEmpty())
        {
            if(!wagonIsSmallerAs(trackA, trackC))
            {
                changeToTrack(trackA, trackC);
            }
            else
            {
                while(wagonIsSmallerAs(trackA, trackC))
                {
                    changeToTrack(trackC, trackB);
                }
                changeToTrack(trackA, trackC);
                
                while (!trackB.isEmpty())
                {
                    while(wagonIsSmallerAs(trackA, trackB) && !wagonIsSmallerAs(trackA, trackC))
                    {
                        changeToTrack(trackA, trackC);
                    }
                    changeToTrack(trackB, trackC);
                }
            }
        }
        
        // print the sortet tracks
        printCurrentInfo();
    }
    
    // print current information
    public void printCurrentInfo() {
        // create printing Stacks and synchronize them
        Wagon saveWagonsArray[] = new Wagon[100];
        int arrayPositionItemsIn = 0;
        int arrayPositionItemsBack = 0;

        // out print for track 'A'
        System.out.println("Wagons in track 'A'");
        while (!trackA.isEmpty())
        {
            saveWagonsArray[arrayPositionItemsIn] = trackA.top();
            arrayPositionItemsIn++;
            System.out.print(("-[" + trackA.top().getWagonNummer() + "]-"));
            trackA.pop();
        }
        
        // put items back
        while (arrayPositionItemsIn != 0)
        {
            trackA.push(saveWagonsArray[arrayPositionItemsBack]);
            arrayPositionItemsBack++;
            arrayPositionItemsIn--;
        }
        System.out.println("");
        arrayPositionItemsBack = 0;

        // out print for track 'B'
        System.out.println("Wagons in track 'B'");
        while (!trackB.isEmpty())
        {
            saveWagonsArray[arrayPositionItemsIn] = trackB.top();
            System.out.print(("-[" + trackB.top().getWagonNummer() + "]-"));
            trackB.pop();
            arrayPositionItemsIn++;
        }
        // put items back
        while (arrayPositionItemsIn != 0)
        {
            trackB.push(saveWagonsArray[arrayPositionItemsBack]);
            arrayPositionItemsBack++;
            arrayPositionItemsIn--;
        }
        System.out.println("");
        arrayPositionItemsBack = 0;

        // out print for track 'C'
        System.out.println("Wagons in track 'C'");
        while (!trackC.isEmpty())
        {
            saveWagonsArray[arrayPositionItemsIn] = trackC.top();
            System.out.print(("-[" + trackC.top().getWagonNummer() + "]-"));
            trackC.pop();
            arrayPositionItemsIn++;
        }
        // put items back
        while (arrayPositionItemsIn != 0)
        {
            trackC.push(saveWagonsArray[arrayPositionItemsBack]);
            arrayPositionItemsBack++;
            arrayPositionItemsIn--;
        }
        System.out.println("");
        arrayPositionItemsBack = 0;

        // out print 'end'
        System.out.println("");
        System.out.println("");
        System.out.println("(print end)");
    }
}
