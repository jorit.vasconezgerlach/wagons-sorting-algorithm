/*
 * AUTHOR: Jorit Vásconez Gerlach
 * ╚ GitLab: @jorit.vasconezgerlach (https://gitlab.com/jorit.vasconezgerlach)
 * ╚ Instagram: @jorit.520 (https://www.instagram.com/jorit.520/)
 */

public class Wagon
{
    // instantiate the wagonNummer variable
    private int wagonNummer;
    
    /*------- Constructor -------*/
    public Wagon(int pWagonNummer) {
        // instantiate the wagonNummer variable
        wagonNummer = pWagonNummer;
        // out print
        System.out.println("╋╋╋╋╋╋╋╋╋╋╋┏┓╋╋╋╋╋┏┓");
        System.out.println("╋╋╋╋╋╋╋╋╋╋┏┛┗┓╋╋╋╋┃┃");
        System.out.println("┏━━┳━┳━━┳━┻┓┏╋━━┳━┛┃");
        System.out.println("┃┏━┫┏┫┃━┫┏┓┃┃┃┃━┫┏┓┃");
        System.out.println("┃┗━┫┃┃┃━┫┏┓┃┗┫┃━┫┗┛┃");
        System.out.println("┗━━┻┛┗━━┻┛┗┻━┻━━┻━━┛");
        System.out.println("•Successfully created the wagon with the number " + wagonNummer);
    }
    
    /*------- get-Methods -------*/
    public int getWagonNummer() {
        return wagonNummer;
    }
}