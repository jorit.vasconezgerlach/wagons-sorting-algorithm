/*
 * AUTHOR: Jorit Vásconez Gerlach
 * ╚ GitLab: @jorit.vasconezgerlach (https://gitlab.com/jorit.vasconezgerlach)
 * ╚ Instagram: @jorit.520 (https://www.instagram.com/jorit.520/)
 */

public class Stack<ContentType> {

  /* --------- Anfang der privaten inneren Klasse -------------- */
  private class StackNode {

    private ContentType content = null;
    private StackNode nextNode = null;

    /**
     * Ein neues Objekt vom Typ StackNode<ContentType> wird erschaffen. <br />
     * Der Inhalt wird per Parameter gesetzt. Der Verweis ist leer.
     * 
     * @param pContent der Inhalt des Knotens
     */
    public StackNode(ContentType pContent) {
      content = pContent;
      nextNode = null;
    }

    /**
     * Der Verweis wird auf das Objekt, das als Parameter uebergeben wird,
     * gesetzt.
     * 
     * @param pNext der Nachfolger des Knotens
     */
    public void setNext(StackNode pNext) {
      nextNode = pNext;
    }

    /**
     * 
     * @return das Objekt, auf das der aktuelle Verweis zeigt
     */
    public StackNode getNext() {
      return nextNode;
    }

    /**
     * @return das Inhaltsobjekt vom Typ ContentType
     */
    public ContentType getContent() {
      return content;
    }
  }

  /* ----------- Ende der privaten inneren Klasse -------------- */

  private StackNode head;

  /**
   * Ein leerer Stapel wird erzeugt. Objekte, die in diesem Stapel verwaltet
   * werden, muessen vom Typ ContentType sein.
   */
  public Stack() {
    head = null;
  }

  /**
   * Die Anfrage liefert den Wert true, wenn der Stapel keine Objekte
   * enthaelt, sonst liefert sie den Wert false.
   * 
   * @return true, falls der Stapel leer ist, sonst false
   */
  public boolean isEmpty() {
    return (head == null);
  }

  /**
   * 
   * Das Objekt pContentType wird oben auf den Stapel gelegt. Falls
   * pContentType gleich null ist, bleibt der Stapel unveraendert.
   * 
   * @param pContent 
   * das einzufuegende Objekt vom Typ ContentType
   */
  public void push(ContentType pContent) {
    if (pContent != null) {
      StackNode node = new StackNode(pContent);
      node.setNext(head);
      head = node;
    }
  }

  /**
   * The object from the top will be removed.
   * If the stack is empty it happens nothing.
   */
  public void pop() {
    if (!isEmpty()) {
      head = head.getNext();
    }
  }

  /**
   * The object from the top will be returned.
   * No changes are made to the stack.
   * If the stack is empty it will be returned null.
   */
  public ContentType top() {
    if (!this.isEmpty()) {
      return head.getContent();
    } else {
      return null;
    }
  }
}